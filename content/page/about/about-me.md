---
title: Rólam
subtitle: Mert az embert is meg kell ismerni
type: page
comments: false
date: 2019-09-12
tags: ["Bemutatkozás"]
---

My name is Leanne. I have the following qualities:

- I breed nice doggies
- I do grooming as well
- And something else

That rug really tied the room together.

### More about me

To be honest, I'm having some trouble remembering right now, so why don't you
just watch [my movie](https://en.wikipedia.org/wiki/The_Big_Lebowski) and it
will answer **all** your questions.
