---
title: Kutyáink
subtitle: That we are proud of...
comments: false
date: 2019-09-12
tags: ["Bemutatkozás"]
---

Egy kis képgaléria:

{{< gallery caption-effect="fade" >}}
  {{< figure link="/img/nice-black-pumi.jpg" caption="Yay, a Pumi!" >}}
  {{< figure link="/img/another-pumi.jpg" caption="Yet another Pumi" >}}
  {{< figure link="/img/two-pumis.jpg" caption="Two Pumis" >}}
{{< /gallery >}}
