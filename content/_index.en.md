---
title:
type: page
comments: false
---

Welcome to our site!

Our kennel is the best in the world, and yada yada yada.

Now, this text above is not something you'd want. It should
be modified to contain some welcoming text for the visitors. This is your
job Leanne, to fill up the pages and particularly this page above.
